# README #

## FOREST PROXIMATE PEOPLE ##
### Filename: FPP_code_publication ###

Title: Forest Proximate People Google Earth Engine Code  
Code author: Sarah Castle  
Project leads: Peter Newton, Andrew Kinzer, Daniel Miller, Johan Oldekop  
Agency: United Nations Food and Agriculture Organization (UN FAO)  
FAO Project leads: Thais Linhares-Juvenal, Leticia Pina, Monica Madrid, Javier de Lamo  
Date: 3 April 2022  
See FAO technical report: Citation: Newton, P., Castle, S.E., Kinzer, A.T., Miller, D.C., Oldekop, J.A., Linhares-Juvenal, T., Pina, L., Madrid, M., de Lamo, J. 2022. The number of forest- and tree-proximate people: a new methodology and global estimates. Background Paper to The State of the World’s Forests 2022. Rome, FAO.  

### Forest-proximate people code description ###

This code estimates the number of people living in or near world forests. The default settings define forests as areas with tree canopy cover of at least 15% with a second layer of mixed shrubs and grassland for tree canopy covers ≤70% (per the Copernicus Global Land Cover classification). Forest proximity is defined by people living in or within proximity of land classified as forests. High density urban areas were excluded. The spatial resolution is 100 meters. Pixel values are presented as people per 100 meters2.

The user may define the forest cover dataset (default: Copernicus Global Land Cover forest classification), population dataset (default: WorldPop), year of analysis (default: 2019), rural areas dataset (default: see rural-urban area extents Google Earth Engine code), and the cutoff distance defining the distance at which populations are considered forest proximate (default: 5 kilometers, or 5 000 meters).  

High density urban areas are excluded. In the default setting, these urban areas were defined as contiguous areas with a total population of at least 50,000 people and comprised of pixels which met at least one of two criteria: either the pixel a) had at least 1,500 people per square km, or b) was classified as “built-up” land use by the CGLC dataset (where “built-up” was defined as land covered by buildings and other manmade structures) in 2019. See rural areas Google Earth Engine code separately for details. Users can generate new rural areas data layer for new year of analysis or with different user definitions of urban/rural lands, or users can import another dataset defining rural areas.  

* Link to Google Earth Engine code snapshot: [https://code.earthengine.google.com/0ba21b30369c71790fa20be890ded926](https://code.earthengine.google.com/0ba21b30369c71790fa20be890ded926) 

* Version: 1.0


### How do I get set up? ###

How to use the code: 

* Lines 1-42: User defined input variables. Please select appropriate variables for your current analysis here.  

* Line 17: Input year of analysis. Current analysis settings are for estimating forest proximate people in the year 2019.  

* Line 18: Input cutoff distance defining the distance at which populations are considered forest proximate in meters. Current analysis set to define forest proximity is as people living in or within 5 kilometers (5 000 meters) of land classified as forest per the Copernicus Global Land Cover dataset. 

* Line 19: Input the rural areas Google Earth Engine asset created by the user or import alternative dataset. High density urban areas are excluded, where these urban areas were defined as contiguous areas with a total population of at least 50,000 people and comprised of pixels which met at least one of two criteria: either the pixel a) had at least 1,500 people per square km, or b) was classified as “built-up” land use by the CGLC dataset (where “built-up” was defined as land covered by buildings and other manmade structures). These high-density urban areas are masked out to generate the non-urban/rural population data layer for use in this analysis. See rural areas Google Earth Engine code separately for details. Users can generate new rural-urban extents data layer for new year of analysis or with different user definitions of urban/rural lands, or users can import another dataset defining rural areas.  

* Line 22: Define name of Google Earth Engine asset to be exported, if desired. Running this task will produce a sharable Google Earth Engine asset of the forest proximate people data layer.  

* Line 25: Define name of forests image export, if desired. Running this task will export a GeoTIFF file of the forest data layer to the user's Google Drive.  

* Line 26: Define name of forest proximate people image export, if desired. Running this task will export a GeoTIFF file of the forest proximate people data layer to the user's Google Drive.  

* Line 27: Define name of forest proximate people data table export, if desired. Running this task will export forest proximate people country-level data export as a csv file to user's Google Drive (each line of the csv file will report a country code and the estimate of the number of forest proximate people in the country).  

* Line 34: Define the population dataset. Default: WorldPop dataset. Must name the current Google Earth Engine WorldPop population data Image Collection path.  

* Line 35: Define the forests dataset. Default: Copernicus Global Land Cover dataset. Must name the current Google Earth Engine Copernicus Global Land Cover data Image Collection path. If using a different land cover dataset to define built up areas or if the classification system changes (currently value=111-126 for forest land cover), the user must edit lines 71-75 and 86-103 in the code with the appropriate classification.  

How to run:

* Summary of set up: Import code into Google Earth Engine to run.  

* Dependencies: Requires user to import a non-urban/rural extents data layer (with high-density urban areas masked out). Google Earth Engine Asset for 2019 (using definitions above): [Rural extents asset - 2019](https://code.earthengine.google.com/?asset=users/saraheb3/ruralPop_GEEasset).  

Notes:

* Note: If there is a runtime error, try re-running the code. Global analysis usually takes several hours to complete.  
  
* Note: Errors may arise in the code for different datasets or years of analysis requiring additional changes beyond line 42 of the code. Detailed comments are provided throughout the code for ease of revision.


### Who do I talk to? ###

* Repo owner or admin: Pengyu Hao ([email](Pengyu.Hao@fao.org))
* Other community or team contact: Leticia Pina ([email](Leticia.Pina@fao.org)), Sarah Castle ([email](saraheb3@illinois.edu))



## TREE PROXIMATE PEOPLE ##

### Filename: TPP_code_publication ###

Title: Tree Proximate People Google Earth Engine Code  
Code author: Sarah Castle  
Project leads: Peter Newton, Andrew Kinzer, Daniel Miller, Johan Oldekop  
Agency: United Nations Food and Agriculture Organization (UN FAO)  
UN FAO Project leads: Thais Linhares-Juvenal, Leticia Pina, Monica Madrid, Javier de Lamo  
Date: 27 March 2022  
See FAO technical report: Citation: Newton, P., Castle, S.E., Kinzer, A.T., Miller, D.C., Oldekop, J.A., Linhares-Juvenal, T., Pina, L., Madrid, M., de Lamo, J. 2022. The number of forest- and tree-proximate people: a new methodology and global estimates. Background Paper to The State of the World’s Forests 2022. Rome, FAO.  

### Tree-proximate people code description ###

This code estimates the number of people living in or near the world’s trees outside of forests on agricultural land or croplands. The default settings define trees outside forests (TOF) as areas with tree cover of at least 10% (per the Copernicus Global Land Cover fractional tree cover data) present on land classified as agricultural (cropland or potential grazing lands per the MODIS land cover dataset) and not otherwise classified as forest per the Copernicus Global Land Cover classification dataset. TOF proximity is defined by people living in or within proximity of land classified as TOFs. High density urban areas were excluded. The spatial resolution is 100 meters. Pixel values are presented as people per 100 meters2.  

The user may define the fractional tree cover dataset (default: Copernicus Global Land Cover fractional tree cover), the minimum and maximum percent tree cover defining trees outside forests (default: minimum 10%, maximum 100%), forest cover dataset (default: Copernicus Global Land Cover forest classifications), population dataset (default: WorldPop), agricultural land cover dataset (default: MODIS Land Cover), year of analysis (default: 2019), rural areas dataset (default: see below, and see rural-urban area extents Google Earth Engine code), and the cutoff distance defining the distance at which populations are considered tree proximate (default: 1 kilometer, or 1 000 meters).  

High density urban areas are excluded. In the default setting, these urban areas were defined as contiguous areas with a total population of at least 50,000 people and comprised of pixels which met at least one of two criteria: either the pixel a) had at least 1,500 people per square km, or b) was classified as “built-up” land use by the CGLC dataset (where “built-up” was defined as land covered by buildings and other manmade structures) in 2019. These high-density urban areas are masked out to generate the non-urban/rural population data layer for use in this analysis. See rural areas Google Earth Engine code separately for details. Users can generate new rural-urban extents data layer for new year of analysis or with different user definitions of urban/rural lands, or users can import another dataset defining rural areas.  

Agricultural lands were defined using the FAO-LCCS2 land use classification layer from MODIS Land Cover (MCD12Q1.006). Croplands were defined as the total of three classifications: 1) “Herbaceous Croplands”: dominated by herbaceous annuals (<2m) with at least 60% cover and a cultivated fraction >60%, 2) “Natural Herbaceous/Croplands Mosaics”: mosaics of small-scale cultivation 40-60% with natural shrub or herbaceous vegetation, and 3) “Forest/Cropland Mosaics”: mosaics of small-scale cultivation 40-60% with >10% natural tree cover. Potential grazing lands were defined as the classification: “Natural Herbaceous”: dominated by herbaceous annuals (<2m) with at least 10% cover. Agricultural land (cropland plus potential grazing land) was defined as the total of the four classifications. The user can define whether to run the analysis for agricultural lands or for croplands only.  

* Link to Google Earth Engine code snapshot:  https://code.earthengine.google.com/6a296102c4cec32b66ca68083ea6659b  

* Version: 1.0  
 
### How do I get set up? ###

How to use the code: 
* Lines 1-50: User defined input variables. Please select appropriate variables for your current analysis here.  

* Line 21: Input year of analysis. Current analysis settings are for estimating forest proximate people in the year 2019.  

* Line 22: Input minimum percent tree cover definition for analysis. Default: 10%.  

* Line 23: Input maximum percent tree cover definition for analysis. Default: 100%.  

* Line 24: Input value (1 or 0) to run analysis for agricultural lands (croplands plus potential grazing lands) or for croplands only. Select 1 if running analysis for agricultural lands (cropland + potential grazing lands). Select 0 if running analysis for croplands only. Any other value will result in an error. If user would like to adjust definition of agricultural lands or croplands, or use a dataset other than MODIS land cover, then the user must edit lines 128-149.  

* Line 26: Input cutoff distance defining the distance at which populations are considered forest proximate in meters. Current analysis set to define forest proximity is as people living in or within 5 kilometers (5 000 meters) of land classified as forest per the Copernicus Global Land Cover dataset.  

* Line 27: Input the rural areas Google Earth Engine asset created by the user or import alternative dataset. High density urban areas are excluded, where these urban areas were defined as contiguous areas with a total population of at least 50,000 people and comprised of pixels which met at least one of two criteria: either the pixel a) had at least 1,500 people per square km, or b) was classified as “built-up” land use by the CGLC dataset (where “built-up” was defined as land covered by buildings and other manmade structures). See rural areas Google Earth Engine code separately for details. Users can generate new rural areas data layer for year of analysis and user definitions of urban/rural lands. Code is designed to accept rural/urban dataset that masked out high-density urban areas. Values of default rural areas dataset are presented as people per 100 meters2 with high-density urban areas (as defined above) masked out.  

* Line 30: Define name of Google Earth Engine asset to be exported, if desired. Running this task will produce a sharable Google Earth Engine asset of the tree proximate people data layer.  

* Line 33: Define name of trees outside forests image export, if desired. Running this task will export a GeoTIFF file of the trees outside forest data layer to the user's Google Drive.  

* Line 34: Define name of tree proximate people image export, if desired. Running this task will export a GeoTIFF file of the tree proximate people data layer to the user's Google Drive.  

* Line 35: Define name of tree proximate people data table export, if desired. Running this task will export tree proximate people country-level data export as a csv file to user's Google Drive (each line of the csv file will report a country code and the estimate of the number of tree proximate people in the country).  

* Line 42: Define the population dataset. Default: WorldPop dataset. Must name the current Google Earth Engine WorldPop population data Image Collection path.  

* Line 43: Define the fractional tree cover and forest cover dataset. Default: Copernicus Global Land Cover dataset. Must name the current Google Earth Engine Copernicus Global Land Cover data Image Collection path. If using different datasets for fractional tree cover and for forest cover, user must revise lines 75-76 and 85-86.  

How to run:

* Summary of set up: Import code into Google Earth Engine to run.   

* Dependencies: Requires user to import a non-urban/rural extents data layer (with high-density urban areas masked out). Google Earth Engine Asset for 2019 (using definitions above): [Rural extents asset - 2019](https://code.earthengine.google.com/?asset=users/saraheb3/ruralPop_GEEasset).  

Notes:

* Note: If there is a runtime error, try re-running the code. Global analysis usually takes several hours to complete.  
  
* Note: Errors may arise in the code for different datasets or years of analysis requiring additional changes beyond line 50 of the code. Detailed comments are provided throughout the code for ease of revision.


### Who do I talk to? ###

* Repo owner or admin: Pengyu Hao ([email](Pengyu.Hao@fao.org))
* Other community or team contact: Leticia Pina ([email](Leticia.Pina@fao.org)), Sarah Castle ([email](saraheb3@illinois.edu))


## RURAL-URBAN AREA EXTENTS ##

### Filename: urban_rural_code_publication ###

Title: Rural/Urban Area Extents Google Earth Engine Code  
Code author: Sarah Castle  
Project leads: Peter Newton, Andrew Kinzer, Daniel Miller, Johan Oldekop  
Agency: United Nations Food and Agriculture Organization (UN FAO)  
FAO Project leads: Thais Linhares-Juvenal, Leticia Pina, Monica Madrid, Javier de Lamo  
Date: 27 March 2022  
See FAO technical report: Citation: Newton, P., Castle, S.E., Kinzer, A.T., Miller, D.C., Oldekop, J.A., Linhares-Juvenal, T., Pina, L., Madrid, M., de Lamo, J. 2022. The number of forest- and tree-proximate people: a new methodology and global estimates. Background Paper to The State of the World’s Forests 2022. Rome, FAO.   

### Rural-urban extents description ###

Description: This code estimates the extent of non-urban/rural areas, excluding high-density urban areas. These high-density urban areas are masked out to generate the non-urban/rural population data layer. This data layer is an input into the Forest Proximate People (FPP) code and Tree Proximate People (TPP) code. The user may define input variables in lines 16-31, and the code exports a Google Earth Engine Asset that should be imported into the FPP and TPP codes when running those, unless another dataset is used to define rural areas.  

In the default setting, high-density urban areas were defined as contiguous areas with a total population of at least 50 000 people and comprised of pixels which met at least one of two criteria: either the pixel a) had at least 1 500 people per square km, or b) was classified as “built-up” land use by the CGLC dataset (where “built-up” was defined as land covered by buildings and other manmade structures) in 2019. These high-density urban areas are masked out to generate the non-urban/rural population data layer for exporting.  The spatial resolution is 100 meters. Pixel values are presented as people per 100 meters2 with high-density urban areas excluded. Users can generate new rural areas data layer for new year of analysis or with different user definitions of urban/rural lands. This default definition is taken from the “Degree of Urbanization” approach to defining urban centers (Dijkstra et al., 2020), which is consistent with other definitions of urban centers (Cattaneo, Nelson and McMenomy, 2021; FAO, 2018; OECD, 2012; Pesaresi et al., 2019). There are many other potential definitions of urban/rural extents that could be applied here. For example, Cattaneo, Nelson and McMenomy (2021) considered contiguous areas with a total population of 20 000 people as urban.  

The user may define the population dataset (default: WorldPop), the year of analysis (default: 2019), the cutoff for urban population density (default: 1 500 people per square kilometer or more as high-density urban), and the cutoff for total population (default: 50 000 people or more in contiguous pixels as high-density urban).  

* References:  
	* Cattaneo, A., Nelson, A. & McMenomy, T. 2021. Global mapping of urban–rural catchment areas reveals unequal access to services. Proceedings of the National Academy of Sciences, 118: e2011990118.
	* Dijkstra, L., Florczyk, A.J., Freire, S., Kemper, T., Melchiorri, M., Pesaresi, M. & Schiavina, M. 2020. Applying the degree of urbanisation to the globe: A new harmonised definition reveals a different picture of global urbanisation. Journal of Urban Economics, 125: 103312.
	* FAO. 2018. Guidelines on defining rural areas and compiling indicators for development policy. Cited October 2021. http://www.fao.org/3/ca6392en/ca6392en.pdf 
	* OECD. 2012. Redefining “Urban”: A New Way to Measure Metropolitan Areas, OECD Publishing. Cited October 2021. http://dx.doi.org/10.1787/9789264174108-en 
	* Pesaresi, M., Florczyk, A., Schiavina, M., Melchiorri, M., & Maffenini, L. 2019. GHS settlement grid, updated and refined REGIO model 2014 in application to GHS-BUILT R2018A and GHS-POP R2019A, multitemporal (1975-1990-2000-2015), R2019A. European Commission, Joint Research Centre (JRC). Cited October 2021. http://data.europa.eu/89h/42e8be89-54ff-464e-be7b-bf9e64da5218 

* Link to Google Earth Engine code snapshot: [https://code.earthengine.google.com/43a762a6c8fead27db40adff0e2a0e34](https://code.earthengine.google.com/43a762a6c8fead27db40adff0e2a0e34)  

* Version: 1.0  

### How do I get set up? ###

How to use the code:  

* Lines 1-31: User defined input variables. Please select appropriate variables for your current analysis here.  
* Line 16: Input year of analysis. Current analysis settings are for estimating rural extents in the year 2019.  
* Line 17: Input the cutoff for urban population density. Default value is 1 500 people per square kilometer or more as high-density urban.  
* Line 18: Input the cutoff for total population. Default value is 50 000 people or more in contiguous pixels as high-density urban.  
* Line 21: Define name of Google Earth Engine asset to be exported. Running this task will produce a sharable Google Earth Engine asset of the rural extents data layer.   
* Line 28: Define the population dataset. Default: WorldPop dataset. Must name the current Google Earth Engine WorldPop population data Image Collection path. May need to edit lines 34-46 if using a different population dataset.  
* Line 29: Define the land cover dataset to be used to identify “built up” land use areas. Default: Copernicus Global Land Cover dataset. Must name the current Google Earth Engine Copernicus Global Land Cover data Image Collection path. If using a different land cover dataset to define built up areas or if the classification system changes (currently value=50 for built-up land use), the user must edit lines 49-57 in the code with the appropriate classification.  

How to run:

* Summary of set up: Import code into Google Earth Engine to run.   
 

Notes:

* Note: If there is a runtime error, try re-running the code. Global analysis usually takes several hours to complete.  
  
* Note: Errors may arise in the code for different datasets or years of analysis requiring additional changes beyond line 31 of the code. Detailed comments are provided throughout the code for ease of revision.


### Who do I talk to? ###

* Repo owner or admin: Pengyu Hao ([email](Pengyu.Hao@fao.org))
* Other community or team contact: Leticia Pina ([email](Leticia.Pina@fao.org)), Sarah Castle ([email](saraheb3@illinois.edu))


